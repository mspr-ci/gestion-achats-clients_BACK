const path = require("path");
const webpack = require("webpack");

module.exports = {
  entry: "./src/index.js",
  mode: "production",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "api.bundle.js"
  },
  target: "node",
  plugins: [new webpack.IgnorePlugin(/^pg-native$/)]
};
