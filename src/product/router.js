const router = require("express").Router();

const db = require("./query");
/**
 * Get all products
 * @route GET /products
 * @returns {object} 200 - A success message
 * @returns {object} 500 - Internal error
 */
router.get("/", db.getProducts);

/**
 * Get product by id
 * @route GET /products/:id
 * @param {String} id.route.required - product id
 * @returns {object} 200 - A success message
 * @returns {object} 500 - Internal error
 */
router.get("/:id", db.getProductById);
/**
 * DELETE product from id
 * @route DELETE /products/:id
 * @param {String} id.route.required - product id
 * @returns {object} 200 - A success message
 * @returns {object} 500 - Internal error
 */
router.delete("/:id", db.deleteProduct);

/**
 * Create a new product
 * @route POST /products
 * @param {String} product.form-data - product desciption
 * @returns {object} 201 - New product's id
 * @returns {object} 500 - Internal error
 */
router.post("/", db.createProduct);
/**
 * Update a product
 * @route PUT /products/:id
 * @param {String} id.route.required - product id
 * @param {String} product.form-data - product desciption
 * @returns {object} 200 - Confirmation message
 * @returns {object} 500 - Internal error
 */
router.put("/:id", db.updateProduct);

module.exports = router;
