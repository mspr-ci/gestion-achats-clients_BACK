require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const router = require("./router");

const host = process.env.APP_HOST;
const port = process.env.APP_PORT;

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.use("/", router);

app.listen(port, host, () => {
  console.log(`Server running on ${host}:${port} !`);
});

module.exports = app;

const swagger_gen = require("./config/swagger");
swagger_gen.swagger(app);
