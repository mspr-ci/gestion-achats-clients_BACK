FROM node:lts-alpine

COPY . app/mspr-ci-back

WORKDIR app/mspr-ci-back

RUN npm install && npm install -g webpack

EXPOSE 3001

CMD npm run build && npm run prod
