const router = require('express').Router()

const db = require('./query')
 /**
         * Get all customers
         * @route GET /customers
         * @returns {object} 200 - A success message
         * @returns {object} 500 - Internal error
         */
router.get('/', db.getCustomers);

 /**
         * Get customer by id
         * @route GET /customers/:id
         * @param {String} id.route.required - user id
         * @returns {object} 200 - A success message
         * @returns {object} 500 - Internal error
         */
router.get('/:id', db.getCustomerById);
 /**
         * DELETE customer from id
         * @route DELETE /customers/:id
         * @param {String} id.route.required - customer id
         * @returns {object} 200 - A success message
         * @returns {object} 500 - Internal error
         */
router.delete('/:id', db.deleteCustomer);

 /**
         * Create a new customer
         * @route POST /customers
         * @param {String} customer.form-data - customer desciption
         * @returns {object} 201 - New user's id
         * @returns {object} 500 - Internal error
         */
router.post('/', db.createCustomer);
 /**
         * Update a customer
         * @route PUT /customers/:id
         * @param {String} id.route.required - customer id
         * @param {String} customer.form-data - customer desciption
         * @returns {object} 200 - Confirmation message
         * @returns {object} 500 - Internal error
         */
router.put('/:id', db.updateCustomer);

module.exports = router;