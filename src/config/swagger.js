const version = require("../../package");

module.exports = {
  swagger: server => {
    const expressSwagger = require("express-swagger-generator")(server);
    let options = {
      swaggerDefinition: {
        info: {
          description: "API customers",
          title: "API",
          version: version.version
        },
        host: process.env.APP_HOST + ":" + process.env.APP_PORT,
        basePath: "/",
        produces: ["application/json"],
        schemes: ["http", "https"],
        securityDefinitions: {
          JWT: {
            type: "apiKey",
            in: "header",
            name: "Authorization",
            description: ""
          }
        }
      },
      basedir: __dirname, //app absolute path
      files: [
        "../customer/router.js",
        "../product/router.js",
        "../order/router.js"
      ] //Path to the API handle folder
    };
    return expressSwagger(options);
  }
};
