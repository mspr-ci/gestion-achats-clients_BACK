require("dotenv").config();
const isSsl = process.env.SSL === "true" ? true : null;
const Pool = require("pg").Pool;
const pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  password: process.env.DB_PASS,
  port: process.env.DB_PORT,
  ssl: isSsl
});

const getProducts = (request, response) => {
  pool.query("SELECT * FROM product", (error, results) => {
    if (error) {
      sendErrorResponseProduct(response, error);
      return;
    }
    response.status(200).json(results.rows);
    return;
  });
};

const getProductById = (request, response) => {
  const id = request.params.id;

  pool.query("SELECT * FROM product WHERE id = $1", [id], (error, results) => {
    if (error) {
      sendErrorResponseProduct(response, error);
      return;
    }
    response.status(200).json(results.rows);
    return;
  });
};

const deleteProduct = (request, response) => {
  const id = parseInt(request.params.id);

  pool.query("DELETE FROM product WHERE id = $1", [id], (error, results) => {
    if (error) {
      sendErrorResponseProduct(response, error);
      return;
    }
    response.status(200).send(`Event deleted with ID: ${id}`);
    return;
  });
};

const createProduct = (request, response) => {
  console.log(request.body);
  const { name, price_HT, quantity } = request.body;
  pool.query(
    'INSERT INTO product (name, "price_HT", quantity) VALUES ($1, $2, $3) returning id',
    [name, price_HT, quantity],
    (error, results) => {
      if (error) {
        sendErrorResponseProduct(response, error);
        return;
      }
      response.status(201).send(results.rows);
      return;
    }
  );
};

const updateProduct = (request, response) => {
  const id = parseInt(request.params.id);
  const { name, price_HT, quantity } = request.body;

  pool.query(
    'UPDATE product SET name = $2, "price_HT" = $3, quantity = $4 WHERE id = $1',
    [id, name, price_HT, quantity],
    (error, results) => {
      if (error) {
        sendErrorResponseProduct(response, error);
        return;
      }
      response.status(200).send(`Customer modified with ID: ${id}`);
      return;
    }
  );
};
const sendErrorResponseProduct = (response, error) => {
  if (error.code === "ETIMEDOUT") {
    response.status(504).json({
      message: "Product - Database connection timed out"
    });
    return;
  }

  response.status(500).json({
    message:
      "Product - Erreur lors de la connection a la base de donnée : " + error
  });
};
module.exports = {
  getProducts,
  getProductById,
  deleteProduct,
  createProduct,
  updateProduct
};
