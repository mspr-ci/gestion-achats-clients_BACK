# gestion-achats-clients_back
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=mspr-ci_gestion-achats-clients_BACK&metric=alert_status)](https://sonarcloud.io/dashboard?id=mspr-ci_gestion-achats-clients_BACK)
[![Depfu](https://badges.depfu.com/badges/a93548b17b5f343714d0effdb799df8c/overview.svg)](https://depfu.com/gitlab/mspr-ci/gestion-achats-clients_BACK?project_id=10636)
## Project setup
```
npm install
```

### Compile the projet
```
node index.js
```

### .Env vars
APP_HOST=  
APP_PORT=  
DB_HOST=  
DB_USER=  
DB_PASS=  
DB_PORT=  
DB_NAME=  

