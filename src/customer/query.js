require("dotenv").config();
const isSsl = process.env.SSL === "true" ? true : null;
const Pool = require("pg").Pool;
const pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  password: process.env.DB_PASS,
  port: process.env.DB_PORT,
  ssl: isSsl
});
const getCustomers = (request, response) => {
  pool.query("SELECT * FROM customer", (error, results) => {
    if (error) {
      sendErrorResponseCustomer(response, error);
      return;
    }
    response.status(200).json(results.rows);
    return;
  });
};

const getCustomerById = (request, response) => {
  const id = request.params.id;

  pool.query("SELECT * FROM customer WHERE id = $1", [id], (error, results) => {
    if (error) {
      sendErrorResponseCustomer(response, error);
      return;
    }
    response.status(200).json(results.rows);
    return;
  });
};

const deleteCustomer = (request, response) => {
  const id = parseInt(request.params.id);

  pool.query("DELETE FROM customer WHERE id = $1", [id], (error, results) => {
    if (error) {
      sendErrorResponseCustomer(response, error);
      return;
    }
    response.status(200).send(`Event deleted with ID: ${id}`);
    return;
  });
};

const createCustomer = (request, response) => {
  console.log(request.body);
  const { firstName, lastName, username, password } = request.body;
  pool.query(
    "INSERT INTO customer (first_name, last_name, username, password) VALUES ($1, $2, $3, $4) returning id",
    [firstName, lastName, username, password],
    (error, results) => {
      if (error) {
        sendErrorResponseCustomer(response, error);
        return;
      }
      response.status(201).send(results.rows);
      return;
    }
  );
};

const updateCustomer = (request, response) => {
  const id = parseInt(request.params.id);
  const { firstName, lastName, username, password } = request.body;

  pool.query(
    "UPDATE customer SET first_name = $2, last_name = $3, username = $4, password = $5 WHERE id = $1",
    [id, firstName, lastName, username, password],
    (error, results) => {
      if (error) {
        sendErrorResponseCustomer(response, error);
        return;
      }
      response.status(200).send(`Customer modified with ID: ${id}`);
      return;
    }
  );
};
const sendErrorResponseCustomer = (response, error) => {
  if (error.code === "ETIMEDOUT") {
    response.status(504).json({
      message: "Customer - Database connection timed out"
    });
    return;
  }

  response.status(500).json({
    message:
      "Customer - Erreur lors de la connection a la base de donnée : " + error
  });
};

module.exports = {
  getCustomers,
  getCustomerById,
  deleteCustomer,
  createCustomer,
  updateCustomer
};
