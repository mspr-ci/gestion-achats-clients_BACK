require("dotenv").config();
const format = require("pg-format");
const isSsl = process.env.SSL === "true" ? true : null;
const Pool = require("pg").Pool;
const pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  password: process.env.DB_PASS,
  port: process.env.DB_PORT,
  ssl: isSsl
});

const getOrders = (request, response) => {
  pool.query(
    'SELECT id_order, id_customer, order_date, id_product, quantity, "price_HT" FROM "order" join order_product on "order".id = order_product.id_order',
    (error, results) => {
      if (error) {
        sendErrorResponseOrder(response, error);
        return;
      }
      response.status(200).json(results.rows);
      return;
    }
  );
};

const getOrderById = (request, response) => {
  const id = request.params.id;

  pool.query(
    'SELECT id_order, id_customer, order_date, id_product, quantity, "price_HT" FROM "order" join order_product on "order".id = order_product.id_order WHERE "order".id = $1',
    [id],
    (error, results) => {
      if (error) {
        sendErrorResponseOrder(response, error);
        return;
      }
      response.status(200).json(results.rows);
      return;
    }
  );
};

const deleteOrder = (request, response) => {
  const id = parseInt(request.params.id);

  pool.query('DELETE FROM "order" WHERE id = $1', [id], (error, results) => {
    if (error) {
      sendErrorResponseOrder(response, error);
      return;
    }
    response.status(200).send(`Order deleted with ID: ${id}`);
    return;
  });
};

const createOrder = (request, response) => {
  console.log(request.body);
  const { customer, listProducts } = request.body;
  pool.query(
    'INSERT INTO "order" (id_customer) VALUES ($1) returning id',
    [customer],
    (error, results) => {
      if (error) {
        sendErrorResponseOrder(response, error);
        return;
      }
      const id_order = results.rows[0];
      const arrayProduct = [];
      listProducts.forEach(element => {
        const temporalArray = [];
        temporalArray.push(id_order.id);
        temporalArray.push(element.product);
        temporalArray.push(element.quantity);
        temporalArray.push(element.price);
        arrayProduct.push(temporalArray);
      });
      const sql = format(
        'INSERT INTO order_product (id_order, id_product, quantity, "price_HT") VALUES %L returning *',
        arrayProduct
      );
      pool.query(sql, (error, results) => {
        if (error) {
          console.log(sql);
          sendErrorResponseOrder(response, error);
          return;
        }
        response.status(201).send(results.rows);
        return;
      });
    }
  );
};
const sendErrorResponseOrder = (response, error) => {
  if (error.code === "ETIMEDOUT") {
    response.status(504).json({
      message: "Order - Database connection timed out"
    });
    return;
  }

  response.status(500).json({
    message:
      "Erreur lors de la connection a la base de donnée pendent une requete Order : " +
      error
  });
};

module.exports = {
  getOrders,
  getOrderById,
  deleteOrder,
  createOrder
};
