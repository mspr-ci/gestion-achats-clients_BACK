const app = require("../src/index"); // Link to your server file
const supertest = require("supertest");
const request = supertest(app);

const firstName = "Théo";
const lastName = "Berthome";
const username = "tberthome";
const password = "1234";

describe("Test Routes customer", () => {
  // POST - Customer
  it("Create a new customer", async done => {
    const res = await request.post("/customers/").send({
      firstName: firstName,
      lastName: lastName,
      username: username,
      password: password
    });
    expect(res.statusCode).toEqual(201);
    expect(res.body[0]).toHaveProperty("id");
    done();
  });

  // POST - customer but it fails
  it("Create a new customer", async done => {
    const res = await request.post("/customers/").send({
      name: "test",
      price_HT: '"jsp trop quoi mettre ici mdr',
      quantity: 2
    });
    expect(res.statusCode).toEqual(500);
    done();
  });

  // GET - Customer
  it("Get all customers", async done => {
    const res = await request.get("/customers");
    expect(res.status).toBe(200);
    done();
  });

  // GET - Customer from id
  it("get customer with id", async done => {
    const res = await request.get("/customers/1");
    expect(res.status).toBe(200);
    expect(res.body[0].id).toEqual(1);
    expect(res.body[0].first_name).toEqual("Théo");
    expect(res.body[0].last_name).toEqual("Berthome");
    expect(res.body[0].username).toEqual("tberthome");
    expect(res.body[0].password).toEqual("1234");
    done();
  });

  // UPDATE - customer
  it("Update customer", async done => {
    const res = await request.put("/customers/1").send({
      firstName: firstName,
      lastName: lastName,
      username: username,
      password: password
    });
    expect(res.statusCode).toEqual(200);
    done();
  });

  // DELETE - Customer
  it("delete customer with id", async done => {
    const res = await request.delete("/customers/2");
    expect(res.status).toBe(200);
    done();
  });
});

describe("Test Routes product", () => {
  // POST - product
  it("Create a new product", async done => {
    const res = await request.post("/products/").send({
      name: "test",
      price_HT: 6,
      quantity: 2
    });
    expect(res.statusCode).toEqual(201);
    done();
  });

  // POST - product but it fails
  it("Create a new product", async done => {
    const res = await request.post("/products/").send({
      name: "test",
      price_HT: '"jsp trop quoi mettre ici mdr',
      quantity: 2
    });
    expect(res.statusCode).toEqual(500);
    done();
  });

  // GET - product
  it("Get all products", async done => {
    const res = await request.get("/products");
    expect(res.status).toBe(200);
    done();
  });

  // GET - product from id
  it("get customer with id", async done => {
    const res = await request.get("/products/1");
    expect(res.status).toBe(200);
    done();
  });

  // UPDATE - product
  it("Update product", async done => {
    const res = await request.put("/products/1").send({
      name: "test",
      price_HT: 6,
      quantity: 2
    });
    expect(res.statusCode).toEqual(200);
    done();
  });

  // DELETE - product
  it("delete product with id", async done => {
    const res = await request.delete("/products/2");
    expect(res.status).toBe(200);
    done();
  });
});

describe("Test Routes order", () => {
  // POST - order
  it("Create a new order", async done => {
    const res = await request.post("/orders/").send({
      customer: 1,
      listProducts: [
        { product: 1, quantity: 2, price: 15 },
        { product: 1, quantity: 2, price: 15 }
      ]
    });
    expect(res.statusCode).toEqual(201);
    done();
  });

  // POST - order but it fails
  it("Create a new order", async done => {
    const res = await request.post("/orders/").send({
      name: "test",
      price_HT: '"jsp trop quoi mettre ici mdr',
      quantity: 2
    });
    expect(res.statusCode).toEqual(500);
    done();
  });

  // GET - order
  it("Get all orders", async done => {
    const res = await request.get("/orders/");
    expect(res.status).toBe(200);
    done();
  });

  // GET - order from id
  it("get order with id", async done => {
    const res = await request.get("/orders/1");
    expect(res.status).toBe(200);
    done();
  });

  // DELETE - order
  it("delete order with id", async done => {
    const res = await request.delete("/orders/2");
    expect(res.status).toBe(200);
    done();
  });
});
