const router = require("express").Router();
const swagger = require("swagger-ui-express");
const swaggerGenerator = require("mgr-swagger-express");
const app = require("./index");

// ROUTES
const customerRoute = require("./customer/router");
const productRoute = require("./product/router");
const orderRoute = require("./order/router");

router.use("/customers", customerRoute);
router.use("/products", productRoute);
router.use("/orders", orderRoute);

module.exports = router;
