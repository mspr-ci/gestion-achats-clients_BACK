const router = require("express").Router();

const db = require("./query");
/**
 * Get all Orders
 * @route GET /orders
 * @returns {object} 200 - A success message
 * @returns {object} 500 - Internal error
 */
router.get("/", db.getOrders);

/**
 * Get order by id
 * @route GET /orders/:id
 * @param {String} id.route.required - order id
 * @returns {object} 200 - A success message
 * @returns {object} 500 - Internal error
 */
router.get("/:id", db.getOrderById);
/**
 * DELETE order from id
 * @route DELETE /orders/:id
 * @param {String} id.route.required - order id
 * @returns {object} 200 - A success message
 * @returns {object} 500 - Internal error
 */
router.delete("/:id", db.deleteOrder);

/**
 * Create a new order
 * @route POST /orders
 * @param {String} customer - customer id
 * @param {String} listProducts.form-data - order products desciption
 * @returns {object} 201 - New order's id
 * @returns {object} 500 - Internal error
 */
router.post("/", db.createOrder);

module.exports = router;
