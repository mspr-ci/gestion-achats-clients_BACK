CREATE DATABASE gestion_achats_clients ENCODING 'UTF8';

CREATE TABLE "customer" (
	"id" 			SERIAL PRIMARY KEY,
	"first_name" 	VARCHAR(32) NOT NULL,
	"last_name" 	VARCHAR(32) NOT NULL,
	"username"		VARCHAR(32) NOT NULL,
	"password"		VARCHAR(50) NOT NULL
);
CREATE TABLE "order" (
	"id"			SERIAL PRIMARY KEY,
	"id_customer"	INTEGER NOT NULL REFERENCES customer (id),
	"order_date"	DATE NOT NULL
);

CREATE TABLE "product" (
	"id"		SERIAL PRIMARY KEY,
	"name"		VARCHAR(32) NOT NULL,
	"price_HT"	DECIMAL NOT NULL,
	"quantity"	INTEGER NOT NULL
);

CREATE TABLE "order_product" (
	"id_order"	INTEGER NOT NULL REFERENCES "order" (id),
	"id_product"	INTEGER NOT NULL REFERENCES product (id),
	"quantity"		INTEGER NOT NULL,
	"price_HT"		DECIMAL NOT NULL
);